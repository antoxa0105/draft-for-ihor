import { Card } from "./card.js";
import { Modal } from "./modal.js";

class VisitCollection {
  constructor() {
    this.list = [];
 }

  setUserData() {
    const userName = document.querySelector(".js-enter-email").value;
    const userPassword = document.querySelector(".js-enter-password").value;
    fetch("https://ajax.test-danit.com/api/v2/cards/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email: userName, password: userPassword }),
    })
      .then((response) => response.text())
      .then((token) => {
        fetch("https://ajax.test-danit.com/api/v2/cards", {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        })
          .then((response) => response.json())
          .then((response) => {
            if (response.length === 0 && this.list.length === 0) {
              document.getElementById("root").insertAdjacentHTML("afterbegin", [
                `<h2 class="no-added-message">No items have been added</h2>`,
              ]);
            } else {
              response.forEach((element) => {
                const { title, description, doctor, bp, age, id, weight } =
                  element;
                const card = new Card(title, description, doctor, bp, age, id, weight, token);
                this.list.push(card);
                card.render();
                document.querySelector(".js-doctor-filter-input").disabled=false;
              });
            }
          });
      });
  }

  filterVisit(e) { 
    const filteredList = this.list.filter((card) => {    
      const doctor = card.doctor;
      return doctor.includes(e.target.value);
    });
    document.querySelectorAll(".card").forEach(element => element.remove())
      filteredList.forEach((card) => card.render());
  }
}
const visitCollection = new VisitCollection ();
const logInBtn = document.querySelector(".js-signUp-btn");
logInBtn.addEventListener("click", () => {
  const modal = new Modal();
  modal.openModal();
  const autBtn = document.querySelector(".js-logIn-btn");
  autBtn.addEventListener("click", async () => { 
    visitCollection.setUserData();
    await modal.closeModal();
    logInBtn.remove();
    document.querySelector(".header").insertAdjacentHTML(
        "beforeend",'<button class= "btn btn-primary me-md-2 js-create-visit-btn">Create visit</button>');
     await createVisit();
  });
});

async function createVisit() {
  const createBtn = document.querySelector(".js-create-visit-btn");
  createBtn.addEventListener("click", () => {
    console.log("Test");
  });
}

const searchInput = document.querySelector(".js-doctor-filter-input");
searchInput.addEventListener("input", visitCollection.filterVisit.bind(visitCollection));

//fetch("https://ajax.test-danit.com/api/v2/cards", {
//  method: 'POST',
//  headers: {
//    'Content-Type': 'application/json',
//    'Authorization': `Bearer 1da25101-8507-4054-9a8b-314e0027bebf`
//  },
//  body: JSON.stringify({
//    title: 'Визит к стоматологу',
//    description: 'Плановый визит',
//    doctor: 'Dentist',
//    bp: '24',
//    age: 23,
//    weight: 70
//  })
//})
//  .then(response => response.json())
//  .then(response => console.log(response))