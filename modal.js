export class Modal {
    openModal () {
      const modal = document.createElement('div')
      modal.classList.add('modal', 'show')
      modal.setAttribute('id', 'myModal');
      modal.setAttribute('tabindex', '-1')
      modal.innerHTML = `
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">Enter your data</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                              <input class="js-enter-email" type="email">
                              <input class="js-enter-password" type="password">
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                              <button type="button" class="btn btn-primary me-md-2 js-logIn-btn">Log In</button>
                            </div>
                          </div>
                        </div>`
      document.body.appendChild(modal)
      this.myModal = new bootstrap.Modal(document.getElementById('myModal'), {})
      this.myModal.show();
      const removeBtn = modal.querySelector(".js-logIn-btn")
      removeBtn.addEventListener('click', this.removeAndClose)
    }

    closeModal() {
        this.myModal.dispose()
        myModal.remove()
    }
  }
  
  export class DeleteModal extends Modal {
   constructor(onRemove) {
       super()
       this.remove = onRemove;
   }

   async removeAndClose() {
    await this.remove()
    this.closeModal()
   }

  }
  