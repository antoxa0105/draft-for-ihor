
export class Card  {
    constructor(title, description, doctor, bp, age, id, weight, token) {
      this.title = title;
      this.description = description;
      this.doctor = doctor;
      this.bp = bp;
      this.age = age;
      this.weight = weight;
      this.id = id;   
      this.token = token;   
    }
    
    render() {
      const cardElement = document.createElement('div')
      cardElement.classList.add('card');
      cardElement.insertAdjacentHTML("afterbegin", [`
      <h2>${this.title}</h2>
      <p>${this.description}</p>
      <span>${this.doctor}</span>
      <span>${this.bp}</span>
      <span>${this.age}</span>
      <span>${this.weight}</span>
      <span>${this.id}</span>
      <button class="removeBtn">Delete</button>
      <button class="editBtn">Edit</button>`]
      )
      cardElement.querySelector(".removeBtn").addEventListener('click', () => {
        fetch(`https://ajax.test-danit.com/api/v2/cards/${this.id}`, {
          method: 'DELETE',
          headers: {
            'Authorization': `Bearer ${this.token}`
          },
        })
        .then (() => {
          cardElement.remove()
          console.log(`Deleted post ${this.id}`);
         })
      })
      document.getElementById('root').append(cardElement)
    }
    
  }